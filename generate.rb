require 'iiif/presentation'
require_relative 'alma_bib_record'

class Generate
  IMAGE_SERVER = ENV.fetch("IIIF_SERVER")
  IMAGE_API_PARAMS = 'full/full/0/default.jpg'

  def self.canvas(image_filename)
    base_uri = "#{IMAGE_SERVER}/#{image_filename}"
    params = {
      service_id: base_uri,
      resource_id_default: "#{base_uri}/#{IMAGE_API_PARAMS}",
      resource_id: "#{base_uri}/#{IMAGE_API_PARAMS}"
    }
    image_resource = IIIF::Presentation::ImageResource.create_image_api_image_resource(params)
    # TODO setting canvas id? uuid? does it matter?
    annotation = IIIF::Presentation::Annotation.new()
    annotation.resource = image_resource
    canvas = IIIF::Presentation::Canvas.new()
    canvas['@id'] = "http://example.com/mycanvasid"
    canvas.width = image_resource['width']
    canvas.height = image_resource['height']
    # TODO Need basic metadata
    canvas.label = "My Canvas for #{image_filename}"

    # Adding annotation to canvas
    canvas.images << annotation

    canvas
  end
end

mms_identifier = ENV.fetch("MMS_ID")
base_directory = "/images".freeze
iiif_api_base = "iiif/2/".freeze

puts "Getting AlmaBibRecord for #{mms_identifier}..."
bib_record = AlmaApiQuery.getBibRecord(mms_identifier)

manifest_seed = {
    '@id' => "http://#{mms_identifier}",
    'label' => "#{bib_record.title} #{bib_record.author}",
    'metadata' => bib_record.metadata
}

manifest = IIIF::Presentation::Manifest.new(manifest_seed)
# We're assuming books, so paged viewer
manifest.viewing_hint = 'paged'

sequence = IIIF::Presentation::Sequence.new()

Dir.children("#{base_directory}/#{mms_identifier}").sort.each do |filename|
  next if filename == '.' or filename == '..'

  puts "Processing image: #{iiif_api_base}#{mms_identifier}__#{filename}..."
  sequence.canvases << Generate.canvas("#{iiif_api_base}#{mms_identifier}__#{filename}")
end

# Add more Canvas objects to sequences here for multi-page objects
manifest.sequences << sequence

# Write to file
File.open("/manifests/#{mms_identifier}-manifest.json","w") do |f|
  f.write(manifest.to_json(pretty: true))
end
