# Makefile for common development tasks
menu:
	@echo 'build: Build manifest generator image'
	@echo 'run: Run manifest generator image'
	@echo 'shell: Start a shell in the manifest generator image'

build:
	@echo 'Building manifest generator image'
	@docker build -t cdl-manifest-generator:latest .

run:
	@echo 'Runnning manifest generator image'
	@sh contrib/run.sh

shell:
	@echo 'Starting shell in manifest generator image'
	@docker run --rm -it --entrypoint /bin/sh --mount type=bind,source="$(CURDIR)",target=/generator cdl-manifest-generator:latest

