# CDL Manifest Generator

### Local Setup

#### Clone repository and basic setup

```sh
git clone https://gitlab.com/ucsdlibrary/development/cdl-manifest-generator.git
cd cdl-manifest-generator
```

#### Build Docker container image

```sh
make build
```

#### Run Docker image

First, create a `.env` file in the root directory of the repository.

The contents of the `.env` file should contain the following environment
variables and their real values:

```sh
ALMA_API_KEY=the-alma-api-key
MMS_ID=an-mms-id-for-testing
IIIF_SERVER=a-valid-iiif-server
```

Then, you can run the script:

```sh
make run
```

#### Run Docker image on Dibs System

We probably want something like the following.

Given a directory of processed tifs, which will make up a single manifest at the
following location:

```
/mnt/sdsc/cantaloupe/PROCESSED_IIIF/991018248009706535/
```

And a local directory we want the manifests written to at the following
location:

```
/src/my/manifests
```

We can run the image like:

```sh
docker run --rm -it \
    --env MMS_ID=991013339359706535 \
    --env ALMA_API_KEY=my-api-key \
    --env IIIF_SERVER=lib-iiif-cdl \
    --mount type=bind,source=/mnt/sdsc/cantaloupe/PROCESSED_IIIF,target=/images \
    --mount type=bind,source=/src/my/manifests,target=/manifests \
    registry.gitlab.com/ucsdlibrary/development/cdl-manifest-generator:stable
```

This can be plugged in to a shell script, pipeline, etc.


