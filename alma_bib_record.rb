##
# AlmaBibRecord is a Dry struct, which affords us to opt-in to metadata retrieved from teh Alma API as needed
# It also has type checking and coercion, should we need to do so.
# see: https://dry-rb.org/gems/dry-struct/1.0/
require "dry-struct"
require "dry-types"
class AlmaBibRecord < Dry::Struct
  Types = Dry.Types
  transform_keys(&:to_sym)

  attribute :title, Types::Strict::String
  attribute :author, Types::Strict::String

  # @return Array of keys for populating metadata field of manifest
  def metadata
    [
      { "label" => "Title", "value" => pretty_title},
      { "label" => "Author", "value" => author}
    ]
  end

  private
  # title is of the form: "title /", usually
  # when appended with author, this makes sense. In isolation, it looks terrible
  def pretty_title
    last_slash_index = title.rindex("/")
    if last_slash_index
      title.slice(0,last_slash_index).strip
    else
      title
    end
  end
end

##
# AlmaApiQuery is a light wrapper around the Alma REST API
# see: https://developers.exlibrisgroup.com/alma/apis/#using
require "net/http"
require "json"
class AlmaApiQuery
  ALMA_API_BASE = "https://api-na.hosted.exlibrisgroup.com".freeze
  BIBS_API = "/almaws/v1/bibs/".freeze

  # Given an mms_identifier, query the Alma /bibs API for metadata
  # see: https://developers.exlibrisgroup.com/alma/apis/docs/bibs/R0VUIC9hbG1hd3MvdjEvYmlicy97bW1zX2lkfQ==/
  # see: https://developers.exlibrisgroup.com/console/?url=/wp-content/uploads/alma/openapi/bibs.json#/Catalog/get
  # @return AlmaBibRecord
  def self.getBibRecord(mms_identifier)
    uri = URI(bib_uri(mms_identifier))
    response = Net::HTTP.start(uri.host, uri.port, use_ssl: true) do |http|
      request = Net::HTTP::Get.new uri.request_uri
      http.request request
    end
    AlmaBibRecord.new(JSON.parse(response.body))
  end

  def self.bib_uri(mms_identifier)
    "#{ALMA_API_BASE}#{BIBS_API}#{mms_identifier}?apikey=#{api_key}&format=json"
  end

  def self.api_key
    @alma_api_key ||= ENV.fetch("ALMA_API_KEY")
  end
end

