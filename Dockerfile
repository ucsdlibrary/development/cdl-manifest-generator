# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=2.7.5
FROM ruby:$RUBY_VERSION-alpine3.14 as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  less \
  git

RUN gem install pry-byebug

RUN mkdir /manifests && mkdir /images

WORKDIR /generator

COPY Gemfile* ./
RUN bundle check || bundle install --jobs "$(nproc)"

COPY . ./

ENTRYPOINT ["bundle", "exec", "ruby", "generate.rb"]
