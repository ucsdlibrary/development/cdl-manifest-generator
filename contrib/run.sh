#!/usr/bin/env sh
	if ! test -d ./tmp ;then
		mkdir tmp
	fi
	if ! test -d ./images ;then
		mkdir images
	fi
	docker run --rm -it \
		--env-file .env \
		--mount type=bind,source="$(pwd)/tmp",target=/manifests \
		--mount type=bind,source="$(pwd)/images",target=/images \
		cdl-manifest-generator:latest
